package com.raxos.toastme;

        import androidx.appcompat.app.AppCompatActivity;

        import android.os.Bundle;
        import android.view.View;
        import android.widget.EditText;
        import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText msg ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        msg = (EditText)findViewById(R.id.editTextMsg);
    }
    public void toast (View view){
        String name = msg.getText().toString();
        Toast.makeText(getApplicationContext(),name,Toast.LENGTH_SHORT).show();
    }
}