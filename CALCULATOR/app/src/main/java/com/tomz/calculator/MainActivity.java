package com.tomz.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Double input_1 , input_2;
    TextView editText;
    Button Button0, Button1, Button2, Button3, Button4, Button5, Button6, Button7, Button8, Button9;
    Button Btn_Addition, Btn_Subtraction, Btn_Multiplication, Btn_Division, Btn_Dot, Btn_Equal, Btn_Delete, Btn_Remainder, Btn_AllClear;
    boolean mAddition, mSubtract, mMultiplication, mDivision, mRemainder, mDecimal ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.TextView);

        Button0 = findViewById(R.id.button0);
        Button1 = findViewById(R.id.button1);
        Button2 = findViewById(R.id.button2);
        Button3 = findViewById(R.id.button3);
        Button4 = findViewById(R.id.button4);
        Button5 = findViewById(R.id.button5);
        Button6 = findViewById(R.id.button6);
        Button7 = findViewById(R.id.button7);
        Button8 = findViewById(R.id.button8);
        Button9 = findViewById(R.id.button9);
        Btn_Addition = findViewById(R.id.buttonAdd);
        Btn_Subtraction = findViewById(R.id.buttonSubtraction);
        Btn_Multiplication = findViewById(R.id.buttonMultiplication);
        Btn_Division = findViewById(R.id.buttonDivide);
        Btn_Dot = findViewById(R.id.buttonDot);
        Btn_Equal = findViewById(R.id.buttonEqual);
        Btn_Delete = findViewById(R.id.buttonClear);
        Btn_AllClear = findViewById(R.id.buttonAllClear);
        Btn_Remainder = findViewById(R.id.buttonRemainder);


        Button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "0");
            }
        });
        Button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "1");
            }
        });
        Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "2");
            }
        });
        Button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "3");
            }
        });
        Button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "4");
            }
        });
        Button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "5");
            }
        });
        Button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "6");
            }
        });
        Button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "7");
            }
        });
        Button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "8");
            }
        });
        Button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(editText.getText() + "9");
            }
        });

    }
}