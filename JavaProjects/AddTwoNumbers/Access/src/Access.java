public class Access {

    public static void main(String[] args){  // TODO: Public = visible to eveyone
                                             // TODO : Private = visible only to the class
                                             // TODO : Protected = visible only to package and all subclass
        sayHello();

        int number1 = 5;
        int number2 = 10;
        System.out.println(largeNumber(number1,number2));
    }
    
    public static void sayHello(){
        System.out.println("Hello Javaa");
    }

    public static int largeNumber(int num1 ,int num2){
        int min;
        if (num1 > num2){
            min =num1;
        }else{
            min =num2;
        }
        return min;

    }

}
