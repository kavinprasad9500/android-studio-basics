public class Compare {

    public static void main(String[] args){

        int number1 = 10;
        int number2 = 10;

        if (number1 > number2){
            System.out.println(number1 + " Is Greater Than " + number2);
        } else if (number1 < number2){
            System.out.println(number2 + " Is Greater Than " + number1);
        } else if (number1 == number2){
            System.out.println(number1 + " Is Equal To Number " + number2);
        } else{
            System.out.println("Please Enter the Numbers Correctly");
        }

    }
}
