public class Array {

    public static void main(String[] args){

        int[] score = new int[50];

        int[] anotherScore = {1, 2, 3, 4, 5, 6};

        score[0] = 1;
        score[2] = 2;

        System.out.println(score[1]);

        for (int i = 0; i < score.length; i++){
            score[i] = i * 10;
        }

//        System.out.println(score[1]);
//        System.out.println(score[2]);
//        System.out.println(score[12]);


        for (int i = 0; i < score.length; i++){
            System.out.println(score[i]);
        }

    }

}
