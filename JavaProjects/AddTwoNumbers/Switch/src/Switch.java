public class Switch {

    public static void main(String[] args){

        int star = 1;

        switch (star){
                case 1:
                    System.out.println("Rating Is 1");
                    break;
                case 2:
                    System.out.println("Rating Is 2");
                    break;
                case 3:
                    System.out.println("Rating Is 3");
                    break;
                case 4:
                    System.out.println("Rating Is 4");
                    break;
                case 5:
                    System.out.println("Rating Is 5");
                    break;
                default:
                    System.out.println("Please Provide Rating 1 to 5");
                    break;
        }

    }
}
