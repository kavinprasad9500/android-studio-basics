package com.kavinprasad;

public class Main {

    public static void main(String[] args){
        Student details = new Student();
        details.name = "Kavin ";
        details.rollNumber = 104055;
        details.age = 18;
        details.print();

        details.name = "Dhanush";
        details.rollNumber = 104056;
        details.age = 19;
        details.print();

        PartTime detail = new PartTime();
        detail.name = "vignesh";
        detail.rollNumber = 104057;
        detail.age = 16;
        detail.parentName = "Name";
        detail.partTime = true;
        detail.print();

    }

}
