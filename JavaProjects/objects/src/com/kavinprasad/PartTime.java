package com.kavinprasad;

public class PartTime extends Student{

    boolean partTime;

    @Override
    public void print(){
        super.print();

        if (partTime){
            System.out.println("He is Part Time Student.");
        } 
        else {
            System.out.println("He is not Part Time Student.");
        }
    }

}
