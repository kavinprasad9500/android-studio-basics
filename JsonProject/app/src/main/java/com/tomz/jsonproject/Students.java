package com.tomz.jsonproject;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Students {

    private String name;
    @SerializedName("courseCount")
    private int course_Count;
    private String email;

    private List<Videos> videos;

    public Course course;

    public Students(String name, int courseCount, String email,Course course,List<Videos>videos) {
        this.name = name;
        this.course_Count = courseCount;
        this.email = email;
        this.course = course;
        this.videos = videos;
    }
}
