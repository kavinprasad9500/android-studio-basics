package com.tomz.jsonproject;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gson gson = new Gson();

//        Students one = new Students("Ashwin",5,"kavinprasad333gmail.com");
//        String json = gson.toJson(one);
//

//        String json = "{\"courseCount\":5,\"email\":\"kavinprasad333gmail.com\",\"name\":\"Ashwin\"}";
//        Students one = gson.fromJson(json,Students.class);
//        Log.i("TEST",one.toString());


        List<Videos>videos = new ArrayList<>();
        videos.add(new Videos("Demo",3));
        videos.add(new Videos("Intro",8));
        videos.add(new Videos("Installation",10));

        Course course = new Course("Kavin","Tomz");
        Students one = new Students("KAVIN PRASAD",5,"kavinprasad9500gmail.com",course,videos);
        String json = gson.toJson(one);

        Log.i ("TEST",json);



//        String json = "{\"course\":{\"description\":\"Tomz\",\"name\":\"Kavin\"},\"courseCount\":5,\"email\":\"kavinprasad9500gmail.com\",\"name\":\"KAVIN PRASAD\"}";
//        Students one = gson.fromJson(json,Students.class);
//
//        Log.i("TEST", one.toString());


    }
}