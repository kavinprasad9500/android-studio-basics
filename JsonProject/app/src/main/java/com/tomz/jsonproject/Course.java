package com.tomz.jsonproject;

public class Course {

    private String name;
    private String description;

    public Course(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
