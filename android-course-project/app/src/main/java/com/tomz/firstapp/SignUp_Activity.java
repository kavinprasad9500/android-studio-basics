package com.tomz.firstapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.regex.Pattern;

public class SignUp_Activity extends AppCompatActivity {
    EditText emailText , passwordText , confirmPassword;
    Button signUpButton;
    TextView errorText , sucessText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.signup_page);

        emailText = findViewById(R.id.emailTxt);
        passwordText = findViewById(R.id.passwordTxt);
        confirmPassword = findViewById(R.id.confirmPasswordTxt);
        signUpButton = findViewById(R.id.signUpButon);
        errorText = findViewById(R.id.errortxt);
        sucessText = findViewById(R.id.sucesstxt);


        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailText.getText().toString();
                String pass = passwordText.getText().toString();
                String confrimPass = confirmPassword.getText().toString();
                String errorMessage = "Please Provide Valid details";
                String success = "SignUp SuccessFully";

                if(vaildEmail(email) & vaildPassword(pass) & vaildConfirmPassword(pass , confrimPass)){
                    Toast.makeText(SignUp_Activity.this, "Sucess", Toast.LENGTH_LONG).show();
                    resetError();
                    successMessage(success);
                }else {
                    setError(errorMessage);
                }
            }
        });

        Log.i("LifeCycle", "onCreate");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("LifeCycle", "onStart");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.i("LifeCycle", "onResume");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.i("LifeCycle", "onPause");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.i("LifeCycle", "onStop");
    }



    private boolean vaildEmail(String email){
        return checkRegex("^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$" , email);
    }
    private boolean vaildPassword(String pass){
        return checkRegex("^(?=.*[a-z]).{8,32}$" , pass);
    }
    private boolean vaildConfirmPassword(String actualPass , String confirmPass){
        return actualPass.equals(confirmPass);
    }

    private void resetError(){
        errorText.setVisibility(View.INVISIBLE);
        errorText.setText("");
    }
    private void setError(String errorMsg){
        errorText.setVisibility(View.VISIBLE);
        errorText.setText(errorMsg);
    }
    private void successMessage(String successMsg){
        sucessText.setVisibility(View.VISIBLE);
        sucessText.setText(successMsg);
    }
    private boolean checkRegex(String pattern , String input){
        Pattern r = Pattern.compile(pattern);
        return r.matcher(input).matches();
    }

}