package com.tomz.firstapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class LogIn_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_page);

        TextView signUpOpen = findViewById(R.id.signUpOpen);

        signUpOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignUp();

            }
        });

    }
    public void openSignUp(){
        Intent intent = new Intent(LogIn_Activity.this,SignUp_Activity.class);
        startActivity(intent);
    }
}
