package com.tomz.recyclercard;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class InstagramAdapter extends RecyclerView.Adapter <InstagramAdapter.InstagramViewHolder>{

    //STEP6.2
    private ArrayList<Instagram>myInstagramList;

    //STEP1
    public static class InstagramViewHolder extends RecyclerView.ViewHolder {
        //STEP4
        public ImageView imageView;
        public TextView textView;


        //STEP2
        public InstagramViewHolder(@NonNull View itemView) {
            super(itemView);


            //STEP4
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
        }
    }

    //STEP6.1
    public InstagramAdapter(ArrayList<Instagram> instagramList){
        myInstagramList = instagramList;
    }

    //STEP3
    @NonNull
    @Override
    public InstagramViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        //STEP%
         View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.instagram,parent,false);
         InstagramViewHolder instagramViewHolder = new InstagramViewHolder(view);
         return instagramViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull InstagramViewHolder instagramViewHolder, int i) {
        //STEP7
        Instagram currentItem =myInstagramList.get(i);
        instagramViewHolder.imageView.setImageResource(currentItem.getMyImageSource());
        instagramViewHolder.textView.setText(currentItem.getText());
    }

    @Override
    public int getItemCount() {
        return myInstagramList.size();
    }

}
