package com.tomz.recyclercard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adaptor;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Instagram> instagramList = new ArrayList<>();
        instagramList.add(new Instagram(R.drawable.image1,"Picture1"));
        instagramList.add(new Instagram(R.drawable.image2,"Picture2"));
        instagramList.add(new Instagram(R.drawable.image3,"Picture3"));
        instagramList.add(new Instagram(R.drawable.image4,"Picture4"));
        instagramList.add(new Instagram(R.drawable.image5,"Picture5"));
        instagramList.add(new Instagram(R.drawable.images,"Picture6"));

        //COnfig RV
        recyclerView = findViewById(R.id.RecyclerCard);
        ///TODO : Will Come Back
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        adaptor = new InstagramAdapter(instagramList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adaptor);


    }
}