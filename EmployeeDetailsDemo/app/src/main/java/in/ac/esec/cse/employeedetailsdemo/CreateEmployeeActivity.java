package in.ac.esec.cse.employeedetailsdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import in.ac.esec.cse.employeedetailsdemo.db.DBHelper;

public class CreateEmployeeActivity extends AppCompatActivity implements View.OnClickListener {
    EditText nameEdittext;
    EditText idEdittext;
    EditText departmentEdittext;
    EditText designationEdittext;
    EditText addressEdittext;
    Button submit;
    Button reset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_employee);
        nameEdittext = findViewById(R.id.employeeName);
        idEdittext = findViewById(R.id.employeeId);
        departmentEdittext = findViewById(R.id.department);
        designationEdittext = findViewById(R.id.designation);
        addressEdittext = findViewById(R.id.address);
        submit = findViewById(R.id.submit);
        reset = findViewById(R.id.reset);
        submit.setOnClickListener(this);
        reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.submit){
            String name = nameEdittext.getText().toString();
            String id = idEdittext.getText().toString();
            String department = departmentEdittext.getText().toString();
            String designation = designationEdittext.getText().toString();
            String address = addressEdittext.getText().toString();
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            dbHelper.insertEmployeeDetails(name,id,department,designation,address);
            Toast.makeText(this, "Data updated successfully", Toast.LENGTH_SHORT).show();
            finish();
        }else  if (view.getId() == R.id.reset){
            resetAllData();
        }

    }

    private void resetAllData() {
        nameEdittext.setText("");
        idEdittext.setText("");
        departmentEdittext.setText("");
        designationEdittext.setText("");
        addressEdittext.setText("");
    }
}