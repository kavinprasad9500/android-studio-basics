package in.ac.esec.cse.employeedetailsdemo.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import in.ac.esec.cse.employeedetailsdemo.model.Employee;

public class DBHelper extends DBHandler{


    public DBHelper(Context context) {
        super(context);
    }

    public void insertEmployeeDetails(String name,String employeeId,String department,String designation,String address){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME_COL,name);
        values.put(EMPLOYEE_ID,employeeId);
        db.insert(EMPLOYEE_TABLE,null,values);

        ContentValues details = new ContentValues();
        details.put(EMPLOYEE_DEPARTMENT,department);
        details.put(EMPLOYEE_DESIGNATION,designation);
        details.put(EMPLOYEE_ID,employeeId);
        details.put(EMPLOYEE_ADDRESS,address);
        db.insert(EMPLOYEE_DETAILS_TABLE,null,details);
        db.close();
    }
    @SuppressLint("Range")
    public List<Employee> getAllEmployees() {
        SQLiteDatabase db = getWritableDatabase();
        List<Employee> employeeList = new ArrayList<>();
        String query = "SELECT * FROM "+EMPLOYEE_TABLE;
        Cursor cursor = db.rawQuery(query,null);
        if (cursor!=null){
            while (cursor.moveToNext()){
                 String name =cursor.getString(cursor.getColumnIndex(NAME_COL));
                 String employeeId =cursor.getString(cursor.getColumnIndex(EMPLOYEE_ID));
                 Employee employee = new Employee(name,employeeId);
                 employeeList.add(employee);
            }
        }

        return employeeList;
    }
}
