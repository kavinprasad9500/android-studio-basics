package in.ac.esec.cse.employeedetailsdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.ac.esec.cse.employeedetailsdemo.model.Employee;
import in.ac.esec.cse.employeedetailsdemo.R;

public class EmployeeListAdapter extends BaseAdapter {
    Context context;
    List<Employee> employeeList;

    public EmployeeListAdapter(Context context, List<Employee> employeeList) {
        this.context = context;
        this.employeeList = employeeList;
    }


    @Override
    public int getCount() {
        return employeeList.size();
    }

    @Override
    public Employee getItem(int i) {
        return employeeList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View contentView, ViewGroup viewGroup) {
        View view = LayoutInflater.from(context).inflate(R.layout.employee_list_item,viewGroup,false);
        TextView name = view.findViewById(R.id.employeeName);
        TextView id = view.findViewById(R.id.employeeId);
        Employee employee = getItem(i);
        name.setText(employee.getName());
        id.setText(employee.getEmployeeID());
        return view;
    }
}
