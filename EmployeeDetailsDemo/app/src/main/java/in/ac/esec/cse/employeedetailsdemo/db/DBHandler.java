package in.ac.esec.cse.employeedetailsdemo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.annotation.Repeatable;

public class DBHandler extends SQLiteOpenHelper {

    protected static final String DB_NAME = "employee";

    // below int is our database version
    protected static final int DB_VERSION = 1;
    // below variable is for our table name.
    protected static final String EMPLOYEE_TABLE = "employee";
    protected static final String EMPLOYEE_DETAILS_TABLE = "employee_details";
    //Column values for employee table
    protected static final String ID_COL = "id";
    protected static final String NAME_COL = "name";
    protected static final String EMPLOYEE_ID = "employee_id";
    //Column values for Employee details table
    protected static final String EMPLOYEE_DESIGNATION = "designation";
    protected static final String EMPLOYEE_DEPARTMENT = "department";
    protected static final String EMPLOYEE_ADDRESS = "addreess";


    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // below method is for creating a database by running a sqlite query
    @Override
    public void onCreate(SQLiteDatabase db) {
        // on below line we are creating
        // an sqlite query and we are
        // setting our column names
        // along with their data types.
        String create_employee_table = "CREATE TABLE " + EMPLOYEE_TABLE + " ("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAME_COL + " TEXT,"
                + EMPLOYEE_ID + " TEXT)";

        String create_employee_details = "CREATE TABLE " + EMPLOYEE_DETAILS_TABLE + " ("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + EMPLOYEE_DESIGNATION + " TEXT,"
                + EMPLOYEE_DEPARTMENT + " TEXT,"
                + EMPLOYEE_ADDRESS + " TEXT,"
                + EMPLOYEE_ID + " TEXT)";

        // at last we are calling a exec sql
        // method to execute above sql query
        db.execSQL(create_employee_table);
        db.execSQL(create_employee_details);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // this method is called to check if the table exists already.
        db.execSQL("DROP TABLE IF EXISTS " + EMPLOYEE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + EMPLOYEE_DETAILS_TABLE);
        onCreate(db);
    }
}