package in.ac.esec.cse.employeedetailsdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import in.ac.esec.cse.employeedetailsdemo.adapter.EmployeeListAdapter;
import in.ac.esec.cse.employeedetailsdemo.db.DBHelper;
import in.ac.esec.cse.employeedetailsdemo.model.Employee;

public class MainActivity extends AppCompatActivity {
    ListView employeeList;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        employeeList = findViewById(R.id.list);
        fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),CreateEmployeeActivity.class);
                startActivity(intent);
            }
        });
        populateEmployeeList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateEmployeeList();
    }

    private void populateEmployeeList() {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        List<Employee> employees = dbHelper.getAllEmployees();
        EmployeeListAdapter adapter = new EmployeeListAdapter(this,employees);
        employeeList.setAdapter(adapter);

    }
}