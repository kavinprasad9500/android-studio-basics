package com.raxos.asynctask;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView textView;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
    }

    public void startTask(View view) {
        LaunchAsyncTask launchAsyncTask = new LaunchAsyncTask();
        launchAsyncTask.execute();
    }

    public class LaunchAsyncTask extends AsyncTask<String ,String ,String> {
        ProgressDialog progressDialog;
        LaunchAsyncTask(){

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            textView.setText("Starting Async Task");
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Loading, Please wait");
            progressDialog.show();
        }

        @Override
        protected String  doInBackground(String[] objects) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(String  o) {
            super.onPostExecute(o);
            progressDialog.dismiss();
            textView.setText("Async Task executed completed");
        }
    };


}