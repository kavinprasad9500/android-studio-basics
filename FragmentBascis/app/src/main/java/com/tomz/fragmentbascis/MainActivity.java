package com.tomz.fragmentbascis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private String[] DATA = new String[]{"Quiz 1","Quiz 2","Quiz 3","Quiz 4","Quiz 5"};
    int quizCount = DATA.length;
    int currentPage = -1;

    TextView pagenumber;
    Button nextButton , perviousButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pagenumber = findViewById(R.id.pageNumber);
        nextButton = findViewById(R.id.nextButton);
        perviousButton = findViewById(R.id.previousButton);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextQuiz();
            }
        });

        perviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevQuiz();
            }
        });
        nextQuiz();
    }
    public void nextQuiz(){
        currentPage += 1;
        if (currentPage > quizCount - 1){
            currentPage = quizCount - 1;
        }
        String title = DATA[currentPage];
        createPage(title);
        updatePageCount();
    }

    public void prevQuiz(){
        currentPage -= 1;
        if (currentPage < 0){
            currentPage = 0;
        }
        String title = DATA[currentPage];
        createPage(title);
        updatePageCount();
    }

    public void createPage(String title){
//        1.Create an fragment manager.
//        2.Set up Transaction.
//        3.Commit Transaction.

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        //      Create an fragment.
        Quiz_Fragment quizFragment = Quiz_Fragment.getInstance(title);
        //      add to transaction.
        transaction.replace(R.id.quizContainer,quizFragment,title);
        
        transaction.commit();
    }
    public void updatePageCount(){
        String pageCount = (currentPage + 1) + "/" + quizCount;
        pagenumber.setText(pageCount);
    }
}