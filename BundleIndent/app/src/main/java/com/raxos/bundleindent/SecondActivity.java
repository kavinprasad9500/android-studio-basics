package com.raxos.bundleindent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView outputName, outputEmail, outputPhone;
    String firstName,lastName,email,phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        outputName=findViewById(R.id.outputName);
        outputEmail=findViewById(R.id.outputEmail);
        outputPhone=findViewById(R.id.outputPhone);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        firstName = intent.getStringExtra(MainActivity.extra_first);
        lastName = intent.getStringExtra(MainActivity.extra_last);
        outputName.setText(firstName + " " + lastName);
        email = intent.getStringExtra(MainActivity.extra_email) ;
        outputEmail.setText(email);
        phone = intent.getStringExtra(MainActivity.extra_phone);
        outputPhone.setText(phone);
    }
}