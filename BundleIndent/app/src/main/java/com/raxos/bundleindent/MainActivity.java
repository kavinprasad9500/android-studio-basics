package com.raxos.bundleindent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    EditText firstName,lastName,email,phone,password,confirmPassword;
    Button registerBtn;
    String first_name,last_name,email_address,phone_number,pass,confirmPass;

    public static final String extra_first = "first_name";
    public static final String extra_last = "last_name";
    public static final String extra_email = "email";
    public static final String extra_phone = "phone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstName = findViewById(R.id.enterFirstName);
        lastName = findViewById(R.id.enterLastName);
        email = findViewById(R.id.enteredEmail);
        phone = findViewById(R.id.enteredMobile);
        password = findViewById(R.id.enterPassword);
        confirmPassword = findViewById(R.id.enterConfirmPassword);
        
        registerBtn = findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                first_name = firstName.getText().toString();
                last_name = lastName.getText().toString();
                email_address = email.getText().toString();
                phone_number = phone.getText().toString();
                pass = password.getText().toString();
                confirmPass = confirmPassword.getText().toString();
                if(isValidEmail(email_address)) {
                    if (isValidPhone(phone_number)) {
                        if (confirmPass.equals(pass)) {
                            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(extra_first, first_name);
                            bundle.putString(extra_last, last_name);
                            bundle.putString(extra_email, email_address);
                            bundle.putString(extra_phone, phone_number);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }else{
                            Toast.makeText(getApplicationContext(), "Check Your Password", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Check Your Mobile Number ", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Check Your Email Number ", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }
    public void clearValue(View view){
        firstName.setText("");
        lastName.setText("");
        email.setText("");
        phone.setText("");
        password.setText("");
        confirmPassword.setText("");
    }
    public static boolean isValidEmail(CharSequence target) {
        String email = target.toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern)){
            return true;
        }else{
            return false;
        }
    }
    public static boolean isValidPassword(CharSequence target){
        return false;
    }
    public static boolean isValidPhone(String phone)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }
    }
    public static boolean isValidName(CharSequence target){
        return false;
    }



}