package com.raxos.listactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import javax.xml.namespace.QName;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);

        String[] name = new String[21];
        name[0] = "Kavin";
        name[1] = "Prasad";
        name[2] = "Tom";
        name[3] = "Ram";
        name[4] = "Rajesh";
        name[5] = "Arun";
        name[6] = "Dinesh";
        name[7] = "Dhakchina";
        name[8] = "Boopathi";
        name[9] = "Bruno";
        name[10] = "Dharun";
        name[11] = "Esakki";
        name[12] = "Yuva";
        name[13] = "Karthi";
        name[14] = "Gokul";
        name[15] = "Bala";
        name[16] = "Dinesh";
        name[17] = "Arun";
        name[18] = "Ind";
        name[19] = "Logesh";
        name[20] = "Vignesh";

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.names_list_item,name);
        listView.setAdapter(stringArrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getApplicationContext(),name[position],Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this,SubActivity.class);
                intent.putExtra("name",name[position]);
                startActivity(intent);
            }

        });
    }
}