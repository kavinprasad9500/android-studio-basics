package com.raxos.listactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class SubActivity extends AppCompatActivity {
TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        textView = findViewById(R.id.textView);
        String name = getIntent().getStringExtra("name");
        textView.setText("Hello "+name);
        Toast.makeText(getApplicationContext(),"Hello "+name,Toast.LENGTH_SHORT).show();
    }
}