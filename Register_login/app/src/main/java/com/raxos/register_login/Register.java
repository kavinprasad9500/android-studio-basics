package com.raxos.register_login;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register extends AppCompatActivity {
    Button b1 ,clear;
    EditText enterFirstName, enterLastname, enterEmail, enterPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        enterFirstName = (EditText) findViewById(R.id.enterFirstName);
        enterLastname = (EditText) findViewById(R.id.enterLastName);
        enterEmail = (EditText) findViewById(R.id.enterMailId);
        enterPassword = (EditText) findViewById(R.id.enterPassword);

        b1 = findViewById(R.id.registerBtn);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register.this,Login.class);
                startActivity(intent);
            }
        });
        clear = findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterFirstName.getText().clear();
                enterLastname.getText().clear();
                enterEmail.getText().clear();
                enterPassword.getText().clear();
            }
        });

    }
}


