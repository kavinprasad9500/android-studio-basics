package com.raxos.register_login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Login extends AppCompatActivity {
    EditText enterMailId , enterPassword;
    Button loginBtn;
    String email , pass;

    public static final String extra = "Hello";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        enterMailId = findViewById(R.id.enterMailId);
        enterPassword = findViewById(R.id.enterPassword);

        loginBtn = findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                email = enterMailId.getText().toString();
                pass = enterPassword.getText().toString();
                if(email.equals("admin@gmail.com") && pass.equals("admin")){
                    Intent intent = new Intent(Login.this,Login_success.class);
                    intent.putExtra(extra,email);
                    startActivity(intent);
               }else{
                   Intent intent = new Intent(Login.this,Login_failed.class);
                   startActivity(intent);
               }



            }
        });
    }
}