package com.raxos.register_login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Login_success extends AppCompatActivity {
    String m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_success);

        Intent intent = getIntent();
        TextView tv1 = (TextView) findViewById(R.id.outText);
        m = intent.getStringExtra(Login.extra);
        tv1.setText(m);

    }
}