package in.portx.jsonparseexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextClock;
import android.widget.TextView;

import java.util.List;

public class UserListAdapter extends BaseAdapter {
    Context context;

    public UserListAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    List<User> userList;
    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public User getItem(int i) {
        return userList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View contentView, ViewGroup viewGroup) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_list_item,viewGroup,false);
        TextView nameTextView = view.findViewById(R.id.nameTextView);
        TextView emailTextView = view.findViewById(R.id.emailTextView);
        TextView genderTextView = view.findViewById(R.id.genderTextView);
        TextView statusTextView = view.findViewById(R.id.statusTextView);
        User user = getItem(i);
        nameTextView.setText("Name   : "+user.getName());
        emailTextView.setText("Email    : "+user.getEmail());
        genderTextView.setText("Gender : "+user.getGender());
        statusTextView.setText(" Status  : "+user.name);
        return view;
    }
}
