package com.raxos.toast_indent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ToastIndent extends AppCompatActivity {
    String m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast_indent);
        Intent intent = getIntent();
        TextView tv1 = (TextView) findViewById(R.id.display2);
        m = intent.getStringExtra(MainActivity.extra);
        tv1.setText("Hello " + m);
        Toast.makeText(getApplicationContext(),m,Toast.LENGTH_SHORT).show();
    }
}