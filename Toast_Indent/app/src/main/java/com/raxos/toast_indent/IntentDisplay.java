package com.raxos.toast_indent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class IntentDisplay extends AppCompatActivity {
    String m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_display);
        Intent intent = getIntent();
        TextView tv1 = (TextView) findViewById(R.id.dispaly1);
        m = intent.getStringExtra(MainActivity.extra);
        tv1.setText(m);
    }
}