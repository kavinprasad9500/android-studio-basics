package com.raxos.toast_indent;

        import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText et1;
    Button b1 ,b2;
    String text;
    public static final String extra = "Hello";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et1 = findViewById(R.id.textMsg);
        b1 = findViewById(R.id.display);
        b2 = findViewById(R.id.toast);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,IntentDisplay.class);
                text = et1.getText().toString();
                intent.putExtra(extra,text);
                startActivity(intent);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ToastIndent.class);
                text = et1.getText().toString();
                intent.putExtra(extra,text);
                startActivity(intent);
            }
        });
    }
}